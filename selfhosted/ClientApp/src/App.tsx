import Layout from "./components/layout/Layout";
import {Route, Routes} from "react-router-dom";
import Login from "./pages/login/Login";
import Register from "./pages/register/Register";
import HomePage from "./pages/home-page/HomePage";
import {useSelector} from "react-redux";
import NotFound from "./pages/not-found/NotFound";
import {useSnackbar, VariantType} from "notistack";
import React, {useEffect} from "react";
import {MessageType} from "./store/uiNotifications";
import {ThemeProvider} from "@mui/material";
import {darkBlueTheme, blueTheme, Themes, purpleTheme, greenTheme} from "./store/theme";

function getSeverityByState(notificationType: MessageType): VariantType | undefined {
    switch (notificationType) {
        case MessageType.Information:
            return "info";
        case MessageType.Warning:
            return "warning";
        case MessageType.Error:
            return "error";
        case MessageType.Success:
            return "success";
        default:
            return undefined;
    }
}

function getTheme(theme: any) {
    switch (theme) {
        case Themes.Blue:
            return blueTheme;
        case Themes.DarkBlue:
            return darkBlueTheme;
        case Themes.Purple:
            return purpleTheme;
        case Themes.Green:
            return greenTheme;
        default:
            return darkBlueTheme;
    }
}

export default function App() {
    const {isLogged} = useSelector(((state: any) => state.auth));
    const uiNotifications = useSelector((state: any) => state.uiNotifications);
    const {theme} = useSelector((state: any) => state.theme);
    const {enqueueSnackbar} = useSnackbar();

    useEffect(() => {
        if (uiNotifications.type === MessageType.Unknown) return;
        const notificationVariant = getSeverityByState(uiNotifications.type);
        enqueueSnackbar(uiNotifications.message, {
            variant: notificationVariant, anchorOrigin: {
                vertical: 'bottom',
                horizontal: 'right',
            }
        });
    }, [uiNotifications]);

    return <>
        <ThemeProvider theme={getTheme(theme)}>
            <Layout>
                <Routes>
                    {isLogged && <Route path="/homepage" element={<HomePage/>}/>}
                    {!isLogged &&
                        <>
                            <Route path="/login" element={<Login/>}/>
                            <Route path="/register" element={<Register/>}/>
                        </>}
                    <Route path="/" element={isLogged ? <HomePage/> : <Login/>}/>
                    <Route path="*" element={<NotFound/>}/>
                </Routes>
            </Layout>
        </ThemeProvider>
    </>;
}