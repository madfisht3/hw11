import classes from "./ErrorLabel.module.css";
import {ErrorRounded} from "@mui/icons-material";
import {useSelector} from "react-redux";

const ErrorLabel = () => {
    const { errorMessage } = useSelector((state: any) => state.loading);

    return <>
        <h1 className={classes.label}>
            <ErrorRounded/> {errorMessage}
        </h1>
    </>;
};

export default ErrorLabel;