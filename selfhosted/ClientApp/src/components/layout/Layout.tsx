import {Box, IconButton, Toolbar, Tooltip, Typography} from "@mui/material";
import AppBar from '@mui/material/AppBar';
import React, {useEffect} from "react";
import {AppRegistrationOutlined, ChangeCircleOutlined, LoginOutlined, LogoutOutlined} from "@mui/icons-material";
import {useDispatch, useSelector} from "react-redux";
import HomeIcon from '@mui/icons-material/Home';

import {useNavigate} from "react-router-dom";
import MatCard from "../mat-card/MatCard";
import ErrorLabel from "../error-label/ErrorLabel";
import LoadingLabel from "../loading-label/LoadingLabel";
import {logoutThunk} from "../../store/authThunks";
import {themeActions} from "../../store/theme";

const Layout = (props: any) => {
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const {isLogged} = useSelector((state: any) => state.auth);
    const {name} = useSelector((state: any) => state.page);
    const {isLoading, hasError} = useSelector((state: any) => state.loading);

    const handleLogout = () => {
        dispatch(logoutThunk());
        navigate("/login");
    };

    const changeThemeHandler = () => {
        dispatch(themeActions.changeTheme());
    };

    useEffect(() => {
        navigate("/");
    }, [isLogged]);

    return (
        <Box sx={{flexGrow: 1}}>
            <AppBar position="static">
                <Toolbar>
                    <div>
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={() => navigate("/")}
                            color="inherit">
                            <HomeIcon/>
                        </IconButton>
                    </div>
                    <Typography variant="h6" component="div" sx={{flexGrow: 1}}>
                        {name}
                    </Typography>
                    <div>
                        <Tooltip title="Change theme">
                            <IconButton
                                size="large"
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={() => changeThemeHandler()}
                                color="inherit">
                                <ChangeCircleOutlined/>
                            </IconButton>
                        </Tooltip>
                    </div>
                    {isLogged &&
                        <div>
                            <Tooltip title="Logout">
                                <IconButton
                                    size="large"
                                    aria-label="account of current user"
                                    aria-controls="menu-appbar"
                                    aria-haspopup="true"
                                    onClick={() => handleLogout()}
                                    color="inherit">
                                    <LogoutOutlined/>
                                </IconButton>
                            </Tooltip>
                        </div>
                    }
                    {!isLogged &&
                        <>
                            <div>
                                <Tooltip title="Register new user">
                                    <IconButton
                                        size="large"
                                        aria-label="account of current user"
                                        aria-controls="menu-appbar"
                                        aria-haspopup="true"
                                        onClick={() => navigate("/register")}
                                        color="inherit">
                                        <AppRegistrationOutlined/>
                                    </IconButton>
                                </Tooltip>
                            </div>
                            <div>
                                <Tooltip title="Sign in">
                                    <IconButton
                                        size="large"
                                        aria-label="account of current user"
                                        aria-controls="menu-appbar"
                                        aria-haspopup="true"
                                        onClick={() => navigate("/login")}
                                        color="inherit">
                                        <LoginOutlined/>
                                    </IconButton>
                                </Tooltip>
                            </div>
                        </>}
                </Toolbar>
            </AppBar>
            <MatCard>
                {hasError ? <ErrorLabel/>
                    : isLoading ? <LoadingLabel/> : props.children}
            </MatCard>
        </Box>
    );
};

export default Layout;