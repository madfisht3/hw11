export class BackendUserInfo {
    constructor(public readonly login: string,
                public readonly password: string) {
    }
}

export let Users = [
    new BackendUserInfo("test", "test"),
    new BackendUserInfo("user", "password")
];

export function DoFakeLogin(login: string, password: string): Promise<{ success: boolean, token: string, expiresIn: Date }> {
    return new Promise<{ success: boolean, token: string, expiresIn: Date }>((resolve, reject) => {
        setTimeout(() => {
            if (Users.filter(u => u.login === login && u.password === password).length > 0) {
                const now = new Date();
                let expiresIn = new Date(now.setMinutes(now.getMinutes() + 2));
                resolve({
                    success: true,
                    token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
                    expiresIn
                });
            } else {
                reject();
            }
        }, 1500);
    });
}

export function DoFakeRegistration(login: string, password: string): Promise<{ success: boolean }> {
    return new Promise<{success: boolean}>((resolve) => {
        setTimeout(() => {
            const existingUser = Users.find(u => u.login === login);
            if (existingUser) {
                resolve({ success: false });
            } else {
                Users.push(new BackendUserInfo(login, password));
                resolve({ success: true });
            }
        }, 2000);
    });
}