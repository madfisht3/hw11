import {useDispatch} from "react-redux";
import {pageActions} from "../../store/page";
import {Button, TextField} from "@mui/material";
import classes from "./Register.module.css";
import {registerThunk} from "../../store/authThunks";
import {MessageType, uiNotificationsActions} from "../../store/uiNotifications";



const Register = () => {
    const dispatch = useDispatch();
    dispatch(pageActions.changePage({name: "Register"}));

    function handleSignUp(event: any) {
        event.preventDefault();

        const login = event.target.login.value;
        const confirmation = event.target.confirmation.value;
        const password = event.target.password.value;

        if (!login || !password) return;

        if (password !== confirmation) {
            dispatch(uiNotificationsActions.pushUiMessage({
                type: MessageType.Warning, message: "The confirmation field and password field are not equal." }));
            return;
        }

        dispatch(registerThunk({ login, password }));
    }

    return <div className={classes.main}>
        <div>
            <form onSubmit={handleSignUp}>
                <div className={classes.formItem}>
                    <TextField
                        required
                        id="login"
                        label="Login"
                        name="login"
                        defaultValue=""
                    />
                </div>

                <div className={classes.formItem}>
                    <TextField
                        id="password"
                        label="Password"
                        name="password"
                        type="password"
                        required
                        defaultValue=""
                    />
                </div>

                <div className={classes.formItem}>
                    <TextField
                        id="confirmation"
                        label="Confirmation"
                        name="confirmation"
                        type="password"
                        required
                        defaultValue=""
                    />
                </div>

                <div className={classes.formItem}>
                    <Button
                        style={{ margin: '0 5px'}}
                        variant="contained"
                        type="submit">
                        Sign Up
                    </Button>
                    <Button style={{ margin: '0 5px'}} variant="contained" type="reset">Reset</Button>
                </div>
            </form>
        </div>
    </div>;
}

export default Register;