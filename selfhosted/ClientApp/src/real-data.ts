import {BackendUserInfo, Users} from "./fake-data";

export async function DoLogin(login: string, password: string): Promise<{ success: boolean, token: string, expiresIn: Date }> {
    try {
        const response = await fetch("/Account/sign-in", {
            method: "POST",
            body: JSON.stringify({ username: login, password: password}),
            headers: {
                "Content-Type": "application/json"
            }});
        
        if (!response.ok) {
            return { success: false, token: "", expiresIn: new Date()};
        }
        
        const result = await response.json();
        return { success: true, token: result.JWT, expiresIn: new Date(result.expiresIn) };
    } catch {
        return { success: false, token: "", expiresIn: new Date()};
    }
}

export async function DoRegistration(login: string, password: string): Promise<{ success: boolean }> {
    try {
        const response = await fetch("/Account/sign-up", {
            method: "POST",
            body: JSON.stringify({ username: login, password: password}),
            headers: {
                "Content-Type": "application/json"
            }});
        return { success: response.ok };
    } catch {
        return { success: false };
    }
}