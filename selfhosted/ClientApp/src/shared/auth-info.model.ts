import UserInfo from "./user-info.model";

export default class AuthInfo {
    constructor(public userInfo: UserInfo | undefined,
                public isLogged: boolean) {
    }
}