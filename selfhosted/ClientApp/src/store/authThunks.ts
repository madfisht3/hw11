import {loadingActions} from "./loading";
import {DoFakeLogin, DoFakeRegistration} from "../fake-data";
import {MessageType, uiNotificationsActions} from "./uiNotifications";
import {authActions} from "./auth";
import UserInfo from "../shared/user-info.model";
import {DoLogin, DoRegistration} from "../real-data";

export const logoutThunk = () => {
    return (dispatch: any) => {
      localStorage.removeItem("authInfo");
      dispatch(authActions.logout());
    };
};

export const loginThunk = (authData: any): any => {
    return async (dispatch: any) => {
        try {
            dispatch(loadingActions.setLoading({ isLoading: true, message: "Signing in..." }));

            const authResult = await DoLogin(authData.login, authData.password);

            if (authResult.success) {
                const userInfo = new UserInfo(authData.login, authResult.token, authResult.expiresIn);
                localStorage.setItem("authInfo", JSON.stringify(userInfo));
                dispatch(authActions.setAuthState({ userInfo: userInfo, isLogged: true }));
                dispatch(uiNotificationsActions.pushUiMessage({ type: MessageType.Success, message: "You are logged in" }));
            } else {
                dispatch(uiNotificationsActions.pushUiMessage({ type: MessageType.Error, message: "Can't login" }));
            }
        } catch {
            dispatch(uiNotificationsActions.pushUiMessage({ type: MessageType.Error, message: "Can't login" }));
        } finally {
            dispatch(loadingActions.setLoading({ isLoading: false }));
        }
    }
};

export const registerThunk = (registerData: any): any => {
    return async (dispatch: any) => {
      try {
          dispatch(loadingActions.setLoading({ isLoading: true, message: "Signing up..." }));

          const registerResult = await DoRegistration(registerData.login, registerData.password);
          if (registerResult.success) {
              dispatch(uiNotificationsActions.pushUiMessage({ type: MessageType.Success, message: "You are registered successfully" }));
          } else {
              dispatch(uiNotificationsActions.pushUiMessage({ type: MessageType.Error, message: "Can't register" }));
          }
      } catch {
          dispatch(uiNotificationsActions.pushUiMessage({ type: MessageType.Error, message: "Can't register" }));
      } finally {
          dispatch(loadingActions.setLoading({ isLoading: false }));
      }
    };
}