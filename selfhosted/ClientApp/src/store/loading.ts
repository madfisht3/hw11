import { createSlice } from "@reduxjs/toolkit";

const defaultErrorMessage = "An error occurred";

const loadingSlice = createSlice({
    name: "auth",
    reducers: {
        setLoading(state: any, action: any) {
            if (action.payload.isLoading) {
                state.loadingMessage = action.payload.message ? action.payload.message : "";
            }

            state.isLoading = action.payload.isLoading;
        },
        setError(state: any, action: any) {
            if (action.payload.hasError) {
                console.log(action.payload.errorMessage);
                state.errorMessage = !action.payload.errorMessage ? defaultErrorMessage : action.payload.errorMessage;
            }

            state.hasError = action.payload.hasError;
        }
    },
    initialState: { isLoading: false, hasError: false, errorMessage: defaultErrorMessage, loadingMessage: "" }
});

export const loadingReducer = loadingSlice.reducer;
export const loadingActions = loadingSlice.actions;
