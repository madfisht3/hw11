import { createSlice } from "@reduxjs/toolkit";

const pageSlice = createSlice({
    name: "page",
    reducers: {
        changePage(state: any, action: any) {
            state.name = action.payload.name;
        }
    },
    initialState: { name: "" }
});

export const pageReducer = pageSlice.reducer;
export const pageActions = pageSlice.actions;
