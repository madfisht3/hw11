import { createSlice } from "@reduxjs/toolkit";

export enum MessageType {
    Error,
    Information,
    Warning,
    Success,
    Unknown
}

const uiNotificationsSlice = createSlice({
    name: "uiNotifications",
    reducers: {
        pushUiMessage(state: any, action: any) {
            state.type = action.payload.type;
            state.message = action.payload.message;
            state.version = state.version+1;
        }
    },
    initialState: { type: MessageType.Unknown, message: "", version: 0 }
});

export const uiNotificationsReducer = uiNotificationsSlice.reducer;
export const uiNotificationsActions = uiNotificationsSlice.actions;
