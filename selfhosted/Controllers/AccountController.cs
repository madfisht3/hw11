using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using selfhosted.Abstractions;
using selfhosted.DTO;

namespace selfhosted.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IAuth _authService;
        private readonly string _secretKey = "jsa9v8a89vua9hvjauhv9aehv9eavj9ae8vj9ajvae98hjv9ae8v";

        public AccountController(IAuth authService)
        {
            _authService = authService;
        }

        [HttpPost("sign-in")]
        public async Task<IActionResult> SignInAsync(SignInRequest request)
        {
            bool result = await _authService.SignInAsync(request.Username, request.Password);
            return result ? Ok(GenerateSignInResponse(request)) : BadRequest();
        }

        private SignInResponse GenerateSignInResponse(SignInRequest request)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, request.Username)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_secretKey));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                "http://127.0.0.1",
                "http://127.0.0.1",
                claims,
                signingCredentials: credentials,
                expires: DateTime.UtcNow.AddHours(8));

            return new SignInResponse
            {
                JWT = new JwtSecurityTokenHandler().WriteToken(token),
                ExpiresIn = token.ValidTo
            };
        }

        [HttpPost("sign-up")]
        public async Task<IActionResult> SignUpAsync(SignUpRequest request)
        {
            var result = await _authService
                .SignUpAsync(request.Username, request.Password);

            return result.Item1
                ? Ok()
                : BadRequest(result.Item2);
        }
    }
}