using System;

namespace selfhosted.DTO
{
    public class SignInResponse
    {
        public string JWT { get; set; }
        public DateTime ExpiresIn { get; set; }
    }
}