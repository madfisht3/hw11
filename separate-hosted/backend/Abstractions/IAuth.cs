using System.Collections.Generic;
using System.Threading.Tasks;

namespace selfhosted.Abstractions
{
    public interface IAuth
    {
        Task<(bool, IEnumerable<string>)> SignUpAsync(string username, string password);
        Task<bool> SignInAsync(string username, string password);
    }
}