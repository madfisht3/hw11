using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using selfhosted.Abstractions;

namespace selfhosted.Services
{
    public class FakeAuth : IAuth
    {
        private readonly Dictionary<string, string> _accounts;
        
        public FakeAuth()
        {
            _accounts = new Dictionary<string, string>
            {
                { "test", "test"},
                { "user", "user"}
            };
        }

        public Task<(bool, IEnumerable<string>)> SignUpAsync(string username, string password)
        {
            bool exist = _accounts.ContainsKey(username.ToLower());
            if (exist)
            {
                return Task.FromResult<(bool, IEnumerable<string>)>((false, new []{ "Account is already exist." }));
            }
            
            _accounts.Add(username.ToLower(), password);
            return Task.FromResult<(bool, IEnumerable<string>)>((true, Array.Empty<string>()));
        }

        public Task<bool> SignInAsync(string username, string password)
        {
            return Task.FromResult(
                _accounts.TryGetValue(username.ToLower(), out string passwordInternal)
                && passwordInternal.Equals(password));
        }
    }
}