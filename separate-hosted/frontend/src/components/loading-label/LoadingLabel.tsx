import classes from "../layout/Layout.module.css";
import {CircularProgress} from "@mui/material";
import React from "react";
import {useSelector} from "react-redux";

const LoadingLabel = () => {
    const { loadingMessage } = useSelector((state: any) => state.loading);

    return <>
        <CircularProgress className={classes.spinner}/>
        <h4 className={classes.header}>{loadingMessage}</h4>
    </>;
};

export default LoadingLabel;