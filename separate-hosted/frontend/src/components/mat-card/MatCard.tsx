import {Card} from "@mui/material";
import classes from "./MatCard.module.css";

const MatCard = (props: any) => {

    return <>
        <Card className={classes.card}>
            {props.children}
        </Card>
    </>
};

export default MatCard;