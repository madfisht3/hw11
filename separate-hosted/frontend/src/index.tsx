import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {index} from './store';
import {Provider} from 'react-redux';
import {HashRouter} from "react-router-dom";
import {SnackbarProvider} from "notistack";

ReactDOM.render(
    <Provider store={index}>
        <HashRouter>
            <SnackbarProvider maxSnack={7}>
                <App/>
            </SnackbarProvider>
        </HashRouter>
    </Provider>
    ,
    document.getElementById('root')
);
