import {useDispatch} from "react-redux";
import {pageActions} from "../../store/page";

const HomePage = () => {
    const dispatch = useDispatch();
    dispatch(pageActions.changePage({ name: "Homepage" }));

    return <>HomePage</>;
}

export default HomePage;