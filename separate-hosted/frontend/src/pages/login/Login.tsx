import {useDispatch} from "react-redux";
import {pageActions} from "../../store/page";
import {Button, TextField} from "@mui/material";
import classes from "./Login.module.css";
import {loginThunk} from "../../store/authThunks";

const Login = () => {
    const dispatch = useDispatch();

    dispatch(pageActions.changePage({name: "Login"}));

    function handleSignIn(event: any) {
        event.preventDefault();

        const login = event.target.login.value;
        const password = event.target.password.value;

        if (!login || !password) return;

        dispatch(loginThunk({login, password}));
    }

    return <div className={classes.main}>
        <div>
            <form onSubmit={handleSignIn}>
                <div className={classes.formItem}>
                    <TextField
                        required
                        id="login"
                        label="Login"
                        name="login"
                        defaultValue=""
                    />
                </div>

                <div className={classes.formItem}>
                    <TextField
                        id="password"
                        label="Password"
                        name="password"
                        type="password"
                        required
                        defaultValue=""
                    />
                </div>

                <div className={classes.formItem}>
                    <Button
                        style={{ margin: '0 5px'}}
                        variant="contained"
                        type="submit">
                        Sign In
                    </Button>
                    <Button style={{ margin: '0 5px'}} variant="contained" type="reset">Reset</Button>
                </div>
            </form>
        </div>
    </div>;
}

export default Login;