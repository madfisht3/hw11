import {useDispatch} from "react-redux";
import {pageActions} from "../../store/page";

const NotFound = () => {
    const dispatch = useDispatch();
    dispatch(pageActions.changePage({ name: "Error" }));

    return <>NotFound</>;
}

export default NotFound;