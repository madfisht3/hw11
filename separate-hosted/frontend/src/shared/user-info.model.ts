export default class UserInfo {
    constructor(public readonly login: string,
                public readonly jwt: string,
                public readonly tokenExpiration: Date) {
    }
}