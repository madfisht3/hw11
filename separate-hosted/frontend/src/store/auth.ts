import { createSlice } from "@reduxjs/toolkit";

const authInitialState = getInitialState();

const authSlice = createSlice({
    name: "auth",
    reducers: {
        logout(state: any) {
            state.isLogged = false;
            state.userInfo = undefined;
        },
        setAuthState(state: any, action: any) {
            state.isLogged = action.payload.isLogged;
            state.userInfo = action.payload.userInfo;
        }
    },
    initialState: authInitialState
});

function getInitialState(): any {
    const json = localStorage.getItem("authInfo");

    if (!json) {
        return { userInfo: undefined, isLogged: false };
    }

    try {
        const userInfo = JSON.parse(json);
        if (userInfo && new Date(userInfo.tokenExpiration) > new Date()) {
            return { userInfo, isLogged: true };
        }
    } catch {
        console.error("Auth initial state: Can't deserialize json.");
    }

    return { userInfo: undefined, isLogged: false };
}

export const authReducer = authSlice.reducer;
export const authActions = authSlice.actions;
