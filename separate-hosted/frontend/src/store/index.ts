import {configureStore} from '@reduxjs/toolkit';
import {authReducer} from './auth'
import {pageReducer} from "./page";
import {loadingReducer} from "./loading";
import {uiNotificationsReducer} from "./uiNotifications";
import {themeReducer} from "./theme";

export const index = configureStore({
    reducer: {
        auth: authReducer,
        page: pageReducer,
        loading: loadingReducer,
        uiNotifications: uiNotificationsReducer,
        theme: themeReducer
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({serializableCheck: false}),
});
