import { createSlice } from "@reduxjs/toolkit";
import {createTheme} from "@mui/material";

declare module '@mui/material/styles' {
    interface Theme {
        status: {
            danger: string;
        };
    }
    interface ThemeOptions {
        status?: {
            danger?: string;
        };
    }
}

export const blueTheme = createTheme({
    palette: {
        primary: {
            main: '#42a5f5',
        },
        secondary: {
            main: '#42a5f5',
        },
    },
});


export const darkBlueTheme = createTheme({
    palette: {
        primary: {
            main: '#01579b',
        },
        secondary: {
            main: '#0288d1',
        },
    },
});

export const purpleTheme = createTheme({
    palette: {
        primary: {
            main: '#7b1fa2',
        },
        secondary: {
            main: '#7b1fa2',
        },
    },
});

export const greenTheme = createTheme({
    palette: {
        primary: {
            main: '#057b22',
        },
        secondary: {
            main: '#057b22',
        },
    }
})

export enum Themes {
    Blue,
    DarkBlue,
    Purple,
    Green
}

const themeSlice = createSlice({
    name: "theme",
    reducers: {
        changeTheme(state: any) {
            switch (state.theme) {
                case Themes.Blue:
                    state.theme = Themes.DarkBlue;
                    break;
                case Themes.DarkBlue:
                    state.theme = Themes.Purple;
                    break;
                case Themes.Purple:
                    state.theme = Themes.Green;
                    break;
                case Themes.Green:
                    state.theme = Themes.Blue
            }
        }
    },
    initialState: { theme: Themes.Purple }
});

export const themeReducer = themeSlice.reducer;
export const themeActions = themeSlice.actions;
